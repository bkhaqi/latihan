class Circle {
    static PI = 3.14;

    constructor(r){
        this.r = r;
    }
    
    area(){
        return Circle.PI * this.r * this.r;
    }


}

let circle1 = new Circle(7);

console.log(circle1.area())