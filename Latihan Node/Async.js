const getUser = (id, cb) => {
    const time = id === 1 ? 3000 : 1000;
    setTimeout(() => {
        const nama = id === 1 ? 'Haqi' : 'Zeus';
        cb({id, nama});
    },time);
}

const firstUser = getUser(1, (result) => {
    console.log(result);
});

const secondUser =  getUser(2, (result) => {
    console.log(result);
});

const hello = 'hello world';
console.log(hello, "selesai")