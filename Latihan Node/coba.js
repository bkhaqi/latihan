const Umur = (id, cb) => {
    const time = id === 1 ? 2000 : 5000;
    setTimeout(() => {
        const word = id === 1 ? 'Umur Alpit adalah 18 tahun' : 'Dia akan meninggal pada usia 30 tahun';
        cb(word);
    },time);
}

const word1 = Umur(1, (result) => {
    console.log(result)
});

const word2 = Umur(2, (result) => {
    console.log(result)
});

setTimeout(() => {
    console.log("Selamat datang di penghitung umur");
  }, "1000")
