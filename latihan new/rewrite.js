let ask = true;

while ( ask ) {
    //Player choice
    let player = prompt("Choose rock, paper, scissor")

    //Computer choice
    let com = Math.floor(Math.random() * 3) + 1;

    //Computer logic
    if(com == 1) {
        com = "rock";
    } else if (com == 2) {
        com = "paper";
    } else if (com == 3) {
        com = "scissor";
    }

    //Rules
    let result = "";

    //Draw 
    if( player == com ) {
        result = "DRAW";

    //Not Draw
    } else if (player == "rock" ) {
        result = (com == "scissor") ? "YOU WIN" : "YOU LOSE";
    } else if ( player == "paper" ) {
        result = (com == "rock") ? "YOU WIN" : "YOU LOSE";
    } else if (player == "scissor") {
        result = (com == "paper") ? "YOU WIN" : "YOU LOSE";
    } else {
        result = "WE CANNOT PROCESS";
    }

    //Showing result 
    alert(" You Choose " + player + " And the com choose " + com + " The result is " + result);
    ask = confirm("Play Again ?")
}

alert("Thanks for playing the games")