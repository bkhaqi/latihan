#### Repository Latihan Javascript 


##### // check version
###### node -v || node --version

##### // list installed versions of node (via nvm)
###### nvm ls

##### // install specific version of node
###### nvm install (version)

##### // set default version of node
###### nvm alias default (version)

##### // switch version of node
###### nvm use (version)
##### // to list available remote versions of node (via nvm)
###### nvm ls-remote