const http = require("http")
const fs = require("fs")

function onRequest(request, response) {
    response.writeHead(200, {"content-Type" : "text/html"});
    fs.readFile("./index.html", null, (error, data) => {
        if(error) {
            response.writeHead(404);
            response.write("file not found");
        } else {
            response.write(data);
        }
        response.end(data);
    } );
};

http.createServer(onRequest).listen(80000)