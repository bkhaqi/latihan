const express = require('express');
const app = express();
const port = 3000;
const path = require('path')

let jsonParser = require('body-parser')

let phoneNum = require("./num");

app.set('view engine','ejs');


app.get("/", (req,res) => {
    res.send("OK")
});

app.get("/product", (req, res) => {
    res.json([
        "Apple",
        "Redmi",
        "Iphone"
    ]);
});

app.get("/coba", (req,res) => {
    res.send("Haloapakabar");
});

app.get("/getphonenum", (req, res) => {
    res.json(phoneNum);
});

app.post("/s-phonenum", jsonParser, (req, res) => {

})

app.get("/html", (req,res) => {
    res.sendFile(path.join(__dirname, '/index.html'));
});

// Calculator
app.post('/calculator', (req, res) => {
  const { operator, num1, num2 } = req.body;
  let result;

  switch (operator) {
    case 'add':
      result = parseInt(num1) + parseInt(num2);
      break;
    case 'subtract':
      result = parseInt(num1) - parseInt(num2);
      break;
    case 'multiply':
      result = parseInt(num1) * parseInt(num2);
      break;
    case 'divide':
      result = parseInt(num1) / parseInt(num2);
      break;
    default:
      result = 'Invalid Operation';
  }

  res.send({ result });
});




app.listen(port,() => {
console.log(`Example port ${port}`);
});