class Rectangle {
    constructor(p){
        this.p = p;
    }

    area(){
       return this.p * this.p;
    }
}
 

class Rectangular extends Rectangle {
    constructor(p,l){
        super(p);
        this.l = l;
    }

    area(){
        return this.p * this.l;
    }
}

let r1 = new Rectangle(6)

console.log(r1.area())