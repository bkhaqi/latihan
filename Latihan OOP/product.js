class product {
   constructor(
    Id,
    Name,
    Description,
    Price,
    Image,
    Discount
   ) {
    this.Id = Id;
    this.Name = Name;
    this.Description = Description;
    this.Price = Price;
    this.Image = Image;
    this.Discount = Discount;
    this.isActive = true;
   }

   //Methods

   getName() {
    return `${this.Name}`
   }
   getDescription(){
    return `${this.Description}`
   }
   getPrice(){
    return `${this.Price}`
   }
   getPriceAfterDiscount(){
    let priceAfter = this.Price - (this.Price - (this.Price * this.Discount / 100));
    return `${priceAfter}`
   }
   setActive(){
    this.isActive = true;
   }
   setInactive(){
    this.isActive = false;
   }
   
   getProductStatus(){
    if (this.isActive === true){
        return `The product is Available`
    } else {
        return `The product is Unavailable`
    }
   }
}

//Implementation
let product1 = new product(
    11232745, 
    "Sego Pecel", 
    "Made in Indonesia", 
    "Rp. 10,000", 
    "Image.jpg", 
    5
    );


    console.table(product1)
    product1.setInactive();
    console.log (product1.getProductStatus())

    

module.export = product;