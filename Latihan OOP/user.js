class user {
    constructor(Id, FirstName, LastName, YearBirth, MonthBirh, DateBirth){
        //Properties
        this.Id = Id;
        this.FirstName = FirstName;
        this.LastName = LastName;
        this.YearBirth = YearBirth;
        this.MonthBirh = MonthBirh;
        this.DateBirth = DateBirth;
        this.isVerified = false;
    }

    //Methods 
    getFullName(){
        return `${this.FirstName} ${this.LastName}`;
    }
    getBirthdayDate() {
        return `${this.YearBirth} ${this.MonthBirh} ${this.DateBirth}`;
    }
    setVerified () {
        this.isVerified = true;
    }
    setUnverified(){
        this.isVerified = false;
    }
    getVerificationStatus(){
        if (this.isVerified === true ) {
            return `This user is verified`;
        } else {
            return `This use is unverified`;
        }
    }
}

    //Implementation
    // let user1 = new user(
    //     1001012,
    //     "Johan",
    //     "Andika",
    //     1990,
    //     12,
    //     13,
    // );
    // user1.setVerified();

    // console.table (user1)
    // console.log("This user Full name is " + user1.getFullName())

module.export = user;